<html>
 <head>
  <title>Video Stats Fetch and Insert</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 </head>
 <body>
 <?php

require 'mysql_config.php';

// Make a MySQL Connection
$conn=mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
mysql_select_db($dbname) or die(mysql_error());
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET COLLATION_CONNECTION = 'utf8_unicode_ci'");

require 'vh_config.php';

$URL='http://www.viralheat.com/api/profile/list_all?api_key='.$api_key;
$doc = new DomDocument();
$doc->load($URL);
$q = new DomXPath($doc);
$profile_entry=0;

foreach ($q->query('//id') as $r) {
$profile = $q->query('//id')->item($profile_entry)->nodeValue;

$URL='https://www.viralheat.com/api/video/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=1';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$oneday_total_videos = mysql_real_escape_string($xml->query('//total_videos')->item($entry)->nodeValue);
$oneday_total_videos_today = mysql_real_escape_string($xml->query('//total_videos_today')->item($entry)->nodeValue);
$oneday_average_videos = mysql_real_escape_string($xml->query('//average_videos')->item($entry)->nodeValue);
$oneday_total_views = mysql_real_escape_string($xml->query('//total_views')->item($entry)->nodeValue);
$oneday_hottest_video_title = mysql_real_escape_string($xml->query('//hottest_video_title')->item($entry)->nodeValue);
$oneday_hottest_video_url = mysql_real_escape_string($xml->query('//hottest_video_url')->item($entry)->nodeValue);
$oneday_hottest_video_views = mysql_real_escape_string($xml->query('//hottest_video_views')->item($entry)->nodeValue);
$oneday_percent_daily_change = mysql_real_escape_string($xml->query('//precent_daily_change')->item($entry)->nodeValue);

$URL='https://www.viralheat.com/api/video/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=7';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$sevenday_total_videos = mysql_real_escape_string($xml->query('//total_videos')->item($entry)->nodeValue);
$sevenday_total_videos_today = mysql_real_escape_string($xml->query('//total_videos_today')->item($entry)->nodeValue);
$sevenday_average_videos = mysql_real_escape_string($xml->query('//average_videos')->item($entry)->nodeValue);
$sevenday_total_views = mysql_real_escape_string($xml->query('//total_views')->item($entry)->nodeValue);
$sevenday_hottest_video_title = mysql_real_escape_string($xml->query('//hottest_video_title')->item($entry)->nodeValue);
$sevenday_hottest_video_url = mysql_real_escape_string($xml->query('//hottest_video_url')->item($entry)->nodeValue);
$sevenday_hottest_video_views = mysql_real_escape_string($xml->query('//hottest_video_views')->item($entry)->nodeValue);
$sevenday_percent_daily_change = mysql_real_escape_string($xml->query('//precent_daily_change')->item($entry)->nodeValue);

$URL='https://www.viralheat.com/api/video/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=30';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$thirtyday_total_videos = mysql_real_escape_string($xml->query('//total_videos')->item($entry)->nodeValue);
$thirtyday_total_videos_today = mysql_real_escape_string($xml->query('//total_videos_today')->item($entry)->nodeValue);
$thirtyday_average_videos = mysql_real_escape_string($xml->query('//average_videos')->item($entry)->nodeValue);
$thirtyday_total_views = mysql_real_escape_string($xml->query('//total_views')->item($entry)->nodeValue);
$thirtyday_hottest_video_title = mysql_real_escape_string($xml->query('//hottest_video_title')->item($entry)->nodeValue);
$thirtyday_hottest_video_url = mysql_real_escape_string($xml->query('//hottest_video_url')->item($entry)->nodeValue);
$thirtyday_hottest_video_views = mysql_real_escape_string($xml->query('//hottest_video_views')->item($entry)->nodeValue);
$thirtyday_percent_daily_change = mysql_real_escape_string($xml->query('//precent_daily_change')->item($entry)->nodeValue);

// query

#$result = mysql_query("SELECT * FROM vh_profile_full_details WHERE profile_id ='$profile_id' and search_term = '$search_expression' and profile_created = '$profile_created' and stats_processed = '$stats_processed' and deliver_email = '$deliver_email' and profile_public = '$profile_public' and profile_category = '$profile_category'");
#$num_rows = mysql_num_rows($result);
#if ($num_rows < 1) {

mysql_query("INSERT INTO vh_video_stats (profile_id, profile_name, search_expression, 1d_total_videos, 1d_total_videos_today, 1d_average_videos, 1d_total_views, 1d_hottest_video_title, 1d_hottest_video_url, 1d_hottest_video_views, 1d_percent_daily_change, 7d_total_videos, 7d_total_videos_today, 7d_average_videos, 7d_total_views, 7d_hottest_video_title, 7d_hottest_video_url, 7d_hottest_video_views, 7d_percent_daily_change, 30d_total_videos, 30d_total_videos_today, 30d_average_videos, 30d_total_views, 30d_hottest_video_title, 30d_hottest_video_url, 30d_hottest_video_views, 30d_percent_daily_change) VALUES ('$profile_id', '$profile_name', '$search_expression', '$oneday_total_videos', '$oneday_total_videos_today', '$oneday_average_videos', '$oneday_total_views', '$oneday_hottest_video_title', '$oneday_hottest_video_url', '$oneday_hottest_video_views', '$oneday_percent_daily_change', '$sevenday_total_videos', '$sevenday_total_videos_today', '$sevenday_average_videos', '$sevenday_total_views', '$sevenday_hottest_video_title', '$sevenday_hottest_video_url', '$sevenday_hottest_video_views', '$sevenday_percent_daily_change', '$thirtyday_total_videos', '$thirtyday_total_videos_today', '$thirtyday_average_videos', '$thirtyday_total_views', '$thirtyday_hottest_video_title', '$thirtyday_hottest_video_url', '$thirtyday_hottest_video_views', '$thirtyday_percent_daily_change')") or die(mysql_error());

echo 'Profile ID: ',$profile_id,'<br>';
echo 'Search expression: ',$search_expression,'<br>';
echo 'Profile name: ',$profile_name,'<br>';
echo 'All stats inserted!';
echo '<br>';

#}

#else {

#}

$profile_entry++;

}

mysql_close($conn);

$to = "simonjohnnicholasgeorge@gmail.com";
 $subject = "VH Update";
 $body = "Hi,\n\nHow are you?\n\nI have just finished running the Video_stats_all_days_fetch script.";
 if (mail($to, $subject, $body)) {
   echo("<p>Message successfully sent!</p>");
  } else {
   echo("<p>Message delivery failed...</p>");
  }

?> 	 

 </body>
</html>