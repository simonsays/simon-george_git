<html>
 <head>
  <title>Twitter Statistics Fetch and Insert</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 </head>
 <body>
 <?php

require 'mysql_config.php';

// Make a MySQL Connection
$conn=mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
mysql_select_db($dbname) or die(mysql_error());
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET COLLATION_CONNECTION = 'utf8_unicode_ci'");

require 'vh_config.php';

$URL='http://www.viralheat.com/api/profile/list_all?api_key='.$api_key;
$doc = new DomDocument();
$doc->load($URL);
$q = new DomXPath($doc);
$profile_entry=0;

foreach ($q->query('//id') as $r) {
$profile = $q->query('//id')->item($profile_entry)->nodeValue;

$URL='https://www.viralheat.com/api/twitter/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=1';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$oneday_total_tweets = mysql_real_escape_string($xml->query('//total_tweets')->item($entry)->nodeValue);
$oneday_average_daily_tweets = mysql_real_escape_string($xml->query('//average_daily_tweets')->item($entry)->nodeValue);
$oneday_total_tweets_today = mysql_real_escape_string($xml->query('//total_tweets_today')->item($entry)->nodeValue);
$oneday_active_day_of_week = mysql_real_escape_string($xml->query('//active_day_of_week')->item($entry)->nodeValue);
$oneday_total_unique_authors = mysql_real_escape_string($xml->query('//total_unique_authors')->item($entry)->nodeValue);
$oneday_popular_author = mysql_real_escape_string($xml->query('//popular_author')->item($entry)->nodeValue);
$oneday_positive_sentiment = mysql_real_escape_string($xml->query('//positive_sentiment')->item($entry)->nodeValue);
$oneday_negative_sentiment = mysql_real_escape_string($xml->query('//negative_sentiment')->item($entry)->nodeValue);
$oneday_neutral_sentiment = mysql_real_escape_string($xml->query('//neutral_sentiment')->item($entry)->nodeValue);
$oneday_total_links = mysql_real_escape_string($xml->query('//total_links')->item($entry)->nodeValue);
$oneday_percent_links = mysql_real_escape_string($xml->query('//percent_links')->item($entry)->nodeValue);
$oneday_total_retweets = mysql_real_escape_string($xml->query('//total_retweets')->item($entry)->nodeValue);
$oneday_percent_retweets = mysql_real_escape_string($xml->query('//percent_retweets')->item($entry)->nodeValue);
$oneday_top_language = mysql_real_escape_string($xml->query('//top_language')->item($entry)->nodeValue);
$oneday_impact = mysql_real_escape_string($xml->query('//impact')->item($entry)->nodeValue);
$oneday_top_author_by_authority = mysql_real_escape_string($xml->query('//top_author_by_authority')->item($entry)->nodeValue);
$oneday_top_author_by_authority_followers = mysql_real_escape_string($xml->query('//top_author_by_authority_followers')->item($entry)->nodeValue);

$URL='https://www.viralheat.com/api/twitter/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=7';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$sevenday_total_tweets = mysql_real_escape_string($xml->query('//total_tweets')->item($entry)->nodeValue);
$sevenday_average_daily_tweets = mysql_real_escape_string($xml->query('//average_daily_tweets')->item($entry)->nodeValue);
$sevenday_total_tweets_today = mysql_real_escape_string($xml->query('//total_tweets_today')->item($entry)->nodeValue);
$sevenday_active_day_of_week = mysql_real_escape_string($xml->query('//active_day_of_week')->item($entry)->nodeValue);
$sevenday_total_unique_authors = mysql_real_escape_string($xml->query('//total_unique_authors')->item($entry)->nodeValue);
$sevenday_popular_author = mysql_real_escape_string($xml->query('//popular_author')->item($entry)->nodeValue);
$sevenday_positive_sentiment = mysql_real_escape_string($xml->query('//positive_sentiment')->item($entry)->nodeValue);
$sevenday_negative_sentiment = mysql_real_escape_string($xml->query('//negative_sentiment')->item($entry)->nodeValue);
$sevenday_neutral_sentiment = mysql_real_escape_string($xml->query('//neutral_sentiment')->item($entry)->nodeValue);
$sevenday_total_links = mysql_real_escape_string($xml->query('//total_links')->item($entry)->nodeValue);
$sevenday_percent_links = mysql_real_escape_string($xml->query('//percent_links')->item($entry)->nodeValue);
$sevenday_total_retweets = mysql_real_escape_string($xml->query('//total_retweets')->item($entry)->nodeValue);
$sevenday_percent_retweets = mysql_real_escape_string($xml->query('//percent_retweets')->item($entry)->nodeValue);
$sevenday_top_language = mysql_real_escape_string($xml->query('//top_language')->item($entry)->nodeValue);
$sevenday_impact = mysql_real_escape_string($xml->query('//impact')->item($entry)->nodeValue);
$sevenday_top_author_by_authority = mysql_real_escape_string($xml->query('//top_author_by_authority')->item($entry)->nodeValue);
$sevenday_top_author_by_authority_followers = mysql_real_escape_string($xml->query('//top_author_by_authority_followers')->item($entry)->nodeValue);

$URL='https://www.viralheat.com/api/twitter/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=30';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$thirtyday_total_tweets = mysql_real_escape_string($xml->query('//total_tweets')->item($entry)->nodeValue);
$thirtyday_average_daily_tweets = mysql_real_escape_string($xml->query('//average_daily_tweets')->item($entry)->nodeValue);
$thirtyday_total_tweets_today = mysql_real_escape_string($xml->query('//total_tweets_today')->item($entry)->nodeValue);
$thirtyday_active_day_of_week = mysql_real_escape_string($xml->query('//active_day_of_week')->item($entry)->nodeValue);
$thirtyday_total_unique_authors = mysql_real_escape_string($xml->query('//total_unique_authors')->item($entry)->nodeValue);
$thirtyday_popular_author = mysql_real_escape_string($xml->query('//popular_author')->item($entry)->nodeValue);
$thirtyday_positive_sentiment = mysql_real_escape_string($xml->query('//positive_sentiment')->item($entry)->nodeValue);
$thirtyday_negative_sentiment = mysql_real_escape_string($xml->query('//negative_sentiment')->item($entry)->nodeValue);
$thirtyday_neutral_sentiment = mysql_real_escape_string($xml->query('//neutral_sentiment')->item($entry)->nodeValue);
$thirtyday_total_links = mysql_real_escape_string($xml->query('//total_links')->item($entry)->nodeValue);
$thirtyday_percent_links = mysql_real_escape_string($xml->query('//percent_links')->item($entry)->nodeValue);
$thirtyday_total_retweets = mysql_real_escape_string($xml->query('//total_retweets')->item($entry)->nodeValue);
$thirtyday_percent_retweets = mysql_real_escape_string($xml->query('//percent_retweets')->item($entry)->nodeValue);
$thirtyday_top_language = mysql_real_escape_string($xml->query('//top_language')->item($entry)->nodeValue);
$thirtyday_impact = mysql_real_escape_string($xml->query('//impact')->item($entry)->nodeValue);
$thirtyday_top_author_by_authority = mysql_real_escape_string($xml->query('//top_author_by_authority')->item($entry)->nodeValue);
$thirtyday_top_author_by_authority_followers = mysql_real_escape_string($xml->query('//top_author_by_authority_followers')->item($entry)->nodeValue);

// query

#$result = mysql_query("SELECT * FROM vh_profile_full_details WHERE profile_id ='$profile_id' and search_term = '$search_expression' and profile_created = '$profile_created' and stats_processed = '$stats_processed' and deliver_email = '$deliver_email' and profile_public = '$profile_public' and profile_category = '$profile_category'");
#$num_rows = mysql_num_rows($result);
#if ($num_rows < 1) {

mysql_query("INSERT INTO vh_twitter_stats (profile_id, profile_name, search_expression, 1d_total_tweets, 1d_average_daily_tweets, 1d_total_tweets_today, 1d_active_dow, 1d_total_unique_authors, 1d_popular_author, 1d_pos_sentiment, 1d_neg_sentiment, 1d_neut_sentiment, 1d_total_links, 1d_percent_links, 1d_total_retweets, 1d_percent_retweets, 1d_top_language, 1d_impact, 1d_top_author_authority, 1d_top_author_authority_followers, 7d_total_tweets, 7d_average_daily_tweets, 7d_total_tweets_today, 7d_active_dow, 7d_total_unique_authors, 7d_popular_author, 7d_pos_sentiment, 7d_neg_sentiment, 7d_neut_sentiment, 7d_total_links, 7d_percent_links, 7d_total_retweets, 7d_percent_retweets, 7d_top_language, 7d_impact, 7d_top_author_authority, 7d_top_author_authority_followers, 30d_total_tweets, 30d_average_daily_tweets, 30d_total_tweets_today, 30d_active_dow, 30d_total_unique_authors, 30d_popular_author, 30d_pos_sentiment, 30d_neg_sentiment, 30d_neut_sentiment, 30d_total_links, 30d_percent_links, 30d_total_retweets, 30d_percent_retweets, 30d_top_language, 30d_impact, 30d_top_author_authority, 30d_top_author_authority_followers) VALUES ('$profile_id', '$profile_name', '$search_expression', '$oneday_total_tweets', '$oneday_average_daily_tweets', '$oneday_total_tweets_today', '$oneday_active_day_of_week', '$oneday_total_unique_authors', '$oneday_popular_author', '$oneday_positive_sentiment', '$oneday_negative_sentiment', '$oneday_neutral_sentiment', '$oneday_total_links', '$oneday_percent_links', '$oneday_total_retweets', '$oneday_percent_retweets', '$oneday_top_language', '$oneday_impact', '$oneday_top_author_by_authority','$oneday_top_author_by_authority_followers', '$sevenday_total_tweets', '$sevenday_average_daily_tweets', '$sevenday_total_tweets_today', '$sevenday_active_day_of_week', '$sevenday_total_unique_authors', '$sevenday_popular_author', '$sevenday_positive_sentiment', '$sevenday_negative_sentiment', '$sevenday_neutral_sentiment', '$sevenday_total_links', '$sevenday_percent_links', '$sevenday_total_retweets', '$sevenday_percent_retweets', '$sevenday_top_language', '$sevenday_impact', '$sevenday_top_author_by_authority','$sevenday_top_author_by_authority_followers', '$thirtyday_total_tweets', '$thirtyday_average_daily_tweets', '$thirtyday_total_tweets_today', '$thirtyday_active_day_of_week', '$thirtyday_total_unique_authors', '$thirtyday_popular_author', '$thirtyday_positive_sentiment', '$thirtyday_negative_sentiment', '$thirtyday_neutral_sentiment', '$thirtyday_total_links', '$thirtyday_percent_links', '$thirtyday_total_retweets', '$thirtyday_percent_retweets', '$thirtyday_top_language', '$thirtyday_impact', '$thirtyday_top_author_by_authority','$thirtyday_top_author_by_authority_followers')") or die(mysql_error());

echo 'Profile ID: ',$profile_id,'<br>';
echo 'Search expression: ',$search_expression,'<br>';
echo 'Profile name: ',$profile_name,'<br>';
echo 'All stats inserted!';
echo '<br>';

#}

#else {

#}

$profile_entry++;

}

mysql_close($conn);

$to = "simonjohnnicholasgeorge@gmail.com";
 $subject = "VH Update";
 $body = "Hi,\n\nHow are you?\n\nI have just finished running the Twitter_stats_all_days_fetch script.";
 if (mail($to, $subject, $body)) {
   echo("<p>Message successfully sent!</p>");
  } else {
   echo("<p>Message delivery failed...</p>");
  }

?> 	 

 </body>
</html>