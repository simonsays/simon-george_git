<?php


# include parseCSV class.
require_once('php/parsecsv.lib.php');


// Create recursive dir iterator which skips dot folders
$dir = new RecursiveDirectoryIterator('vh_csv_dump',
    FilesystemIterator::SKIP_DOTS);

// Flatten the recursive iterator, folders come before their files
$it  = new RecursiveIteratorIterator($dir,
    RecursiveIteratorIterator::SELF_FIRST);

// Maximum depth is 1 level deeper than the base folder
$it->setMaxDepth(2);

// Basic loop displaying different messages based on file or folder
foreach ($it as $fileinfo) {
    if ($fileinfo->isDir()) {
        printf("Folder - %s\n", $fileinfo->getFilename());
        echo "<br>";
    } elseif ($fileinfo->isFile()) {
        printf("File From %s - %s\n", $it->getSubPath(), $fileinfo->getFilename());
        echo "<br>";
    }



# create new parseCSV object.
$csv = new parseCSV();


# Parse '_books.csv' using automatic delimiter detection...
$csv->auto($fileinfo->getFilename());

# ...or if you know the delimiter, set the delimiter character
# if its not the default comma...
// $csv->delimiter = "\t";   # tab delimited

# ...and then use the parse() function.
// $csv->parse('_books.csv');


# Output result.
print_r($csv->data);
echo "<br><br>";


foreach ($csv->titles as $value) {
print_r($value);
echo '<br>';
}

foreach ($csv->data as $key => $row) {

foreach ($row as $value) {
print_r($value);
echo '<br>';
}

}

}
?>