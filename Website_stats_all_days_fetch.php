<html>
 <head>
  <title>Website Stats Fetch and Insert</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 </head>
 <body>
 <?php

require 'mysql_config.php';

// Make a MySQL Connection
$conn=mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
mysql_select_db($dbname) or die(mysql_error());
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET COLLATION_CONNECTION = 'utf8_unicode_ci'");

require 'vh_config.php';

$URL='http://www.viralheat.com/api/profile/list_all?api_key='.$api_key;
$doc = new DomDocument();
$doc->load($URL);
$q = new DomXPath($doc);
$profile_entry=0;

foreach ($q->query('//id') as $r) {
$profile = $q->query('//id')->item($profile_entry)->nodeValue;

$URL='https://www.viralheat.com/api/website/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=1';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$oneday_total_sites = mysql_real_escape_string($xml->query('//total_sites')->item($entry)->nodeValue);
$oneday_average_daily_sites = mysql_real_escape_string($xml->query('//average_daily_sites')->item($entry)->nodeValue);
$oneday_total_sites_today = mysql_real_escape_string($xml->query('//total_sites_today')->item($entry)->nodeValue);
$oneday_top_country = mysql_real_escape_string($xml->query('//top_country')->item($entry)->nodeValue);
$oneday_total_sites_top_country = mysql_real_escape_string($xml->query('//total_sites_top_country')->item($entry)->nodeValue);
$oneday_top_domain = mysql_real_escape_string($xml->query('//top_domain')->item($entry)->nodeValue);
$oneday_total_mentions_top_domain = mysql_real_escape_string($xml->query('//total_mentions_top_domain')->item($entry)->nodeValue);
$oneday_top_domain_url = mysql_real_escape_string($xml->query('//top_domain_url')->item($entry)->nodeValue);
$oneday_top_link = mysql_real_escape_string($xml->query('//top_link')->item($entry)->nodeValue);
$oneday_total_mentions_top_link = mysql_real_escape_string($xml->query('//total_mentions_top_link')->item($entry)->nodeValue);
$oneday_top_link_url = mysql_real_escape_string($xml->query('//top_link_url')->item($entry)->nodeValue);

$URL='https://www.viralheat.com/api/website/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=7';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$sevenday_total_sites = mysql_real_escape_string($xml->query('//total_sites')->item($entry)->nodeValue);
$sevenday_average_daily_sites = mysql_real_escape_string($xml->query('//average_daily_sites')->item($entry)->nodeValue);
$sevenday_total_sites_today = mysql_real_escape_string($xml->query('//total_sites_today')->item($entry)->nodeValue);
$sevenday_top_country = mysql_real_escape_string($xml->query('//top_country')->item($entry)->nodeValue);
$sevenday_total_sites_top_country = mysql_real_escape_string($xml->query('//total_sites_top_country')->item($entry)->nodeValue);
$sevenday_top_domain = mysql_real_escape_string($xml->query('//top_domain')->item($entry)->nodeValue);
$sevenday_total_mentions_top_domain = mysql_real_escape_string($xml->query('//total_mentions_top_domain')->item($entry)->nodeValue);
$sevenday_top_domain_url = mysql_real_escape_string($xml->query('//top_domain_url')->item($entry)->nodeValue);
$sevenday_top_link = mysql_real_escape_string($xml->query('//top_link')->item($entry)->nodeValue);
$sevenday_total_mentions_top_link = mysql_real_escape_string($xml->query('//total_mentions_top_link')->item($entry)->nodeValue);
$sevenday_top_link_url = mysql_real_escape_string($xml->query('//top_link_url')->item($entry)->nodeValue);

$URL='https://www.viralheat.com/api/website/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=30';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$thirtyday_total_sites = mysql_real_escape_string($xml->query('//total_sites')->item($entry)->nodeValue);
$thirtyday_average_daily_sites = mysql_real_escape_string($xml->query('//average_daily_sites')->item($entry)->nodeValue);
$thirtyday_total_sites_today = mysql_real_escape_string($xml->query('//total_sites_today')->item($entry)->nodeValue);
$thirtyday_top_country = mysql_real_escape_string($xml->query('//top_country')->item($entry)->nodeValue);
$thirtyday_total_sites_top_country = mysql_real_escape_string($xml->query('//total_sites_top_country')->item($entry)->nodeValue);
$thirtyday_top_domain = mysql_real_escape_string($xml->query('//top_domain')->item($entry)->nodeValue);
$thirtyday_total_mentions_top_domain = mysql_real_escape_string($xml->query('//total_mentions_top_domain')->item($entry)->nodeValue);
$thirtyday_top_domain_url = mysql_real_escape_string($xml->query('//top_domain_url')->item($entry)->nodeValue);
$thirtyday_top_link = mysql_real_escape_string($xml->query('//top_link')->item($entry)->nodeValue);
$thirtyday_total_mentions_top_link = mysql_real_escape_string($xml->query('//total_mentions_top_link')->item($entry)->nodeValue);
$thirtyday_top_link_url = mysql_real_escape_string($xml->query('//top_link_url')->item($entry)->nodeValue);

// query

#$result = mysql_query("SELECT * FROM vh_profile_full_details WHERE profile_id ='$profile_id' and search_term = '$search_expression' and profile_created = '$profile_created' and stats_processed = '$stats_processed' and deliver_email = '$deliver_email' and profile_public = '$profile_public' and profile_category = '$profile_category'");
#$num_rows = mysql_num_rows($result);
#if ($num_rows < 1) {

mysql_query("INSERT INTO vh_website_stats (profile_id, profile_name, search_expression, 1d_total_sites, 1d_average_daily_sites, 1d_total_sites_today, 1d_top_country, 1d_total_sites_top_country, 1d_top_domain, 1d_total_mentions_top_domain, 1d_top_domain_url, 1d_top_link, 1d_total_mentions_top_link, 1d_top_link_url, 7d_total_sites, 7d_average_daily_sites, 7d_total_sites_today, 7d_top_country, 7d_total_sites_top_country, 7d_top_domain, 7d_total_mentions_top_domain, 7d_top_domain_url, 7d_top_link, 7d_total_mentions_top_link, 7d_top_link_url, 30d_total_sites, 30d_average_daily_sites, 30d_total_sites_today, 30d_top_country, 30d_total_sites_top_country, 30d_top_domain, 30d_total_mentions_top_domain, 30d_top_domain_url, 30d_top_link, 30d_total_mentions_top_link, 30d_top_link_url) VALUES ('$profile_id', '$profile_name', '$search_expression', '$oneday_total_sites', '$oneday_average_daily_sites', '$oneday_total_sites_today', '$oneday_top_country', '$oneday_total_sites_top_country', '$oneday_top_domain', '$oneday_total_mentions_top_domain', '$oneday_top_domain_url', '$oneday_top_link', '$oneday_total_mentions_top_link', '$oneday_top_link_url', '$sevenday_total_sites', '$sevenday_average_daily_sites', '$sevenday_total_sites_today', '$sevenday_top_country', '$sevenday_total_sites_top_country', '$sevenday_top_domain', '$sevenday_total_mentions_top_domain', '$sevenday_top_domain_url', '$sevenday_top_link', '$sevenday_total_mentions_top_link', '$sevenday_top_link_url', '$thirtyday_total_sites', '$thirtyday_average_daily_sites', '$thirtyday_total_sites_today', '$thirtyday_top_country', '$thirtyday_total_sites_top_country', '$thirtyday_top_domain', '$thirtyday_total_mentions_top_domain', '$thirtyday_top_domain_url', '$thirtyday_top_link', '$thirtyday_total_mentions_top_link', '$thirtyday_top_link_url')") or die(mysql_error());

echo 'Profile ID: ',$profile_id,'<br>';
echo 'Search expression: ',$search_expression,'<br>';
echo 'Profile name: ',$profile_name,'<br>';
echo 'All stats inserted!';
echo '<br>';

#}

#else {

#}

$profile_entry++;

}

mysql_close($conn);

$to = "simonjohnnicholasgeorge@gmail.com";
 $subject = "VH Update";
 $body = "Hi,\n\nHow are you?\n\nI have just finished running the Website_stats_all_days_fetch script.";
 if (mail($to, $subject, $body)) {
   echo("<p>Message successfully sent!</p>");
  } else {
   echo("<p>Message delivery failed...</p>");
  }

?> 	 

 </body>
</html>