<html>
 <head>
  <title>Facebook Statistics Fetch and Insert</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 </head>
 <body>
 <?php

require 'mysql_config.php';

// Make a MySQL Connection
$conn=mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
mysql_select_db($dbname) or die(mysql_error());
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET COLLATION_CONNECTION = 'utf8_unicode_ci'");

require 'vh_config.php';

$URL='http://www.viralheat.com/api/profile/list_all?api_key='.$api_key;
$doc = new DomDocument();
$doc->load($URL);
$q = new DomXPath($doc);
$profile_entry=0;

foreach ($q->query('//id') as $r) {
$profile = $q->query('//id')->item($profile_entry)->nodeValue;

$URL='https://www.viralheat.com/api/facebook/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=1';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$oneday_total_facebook_comments = mysql_real_escape_string($xml->query('//total_facebook_comments')->item($entry)->nodeValue);
$oneday_average_facebook_comments = mysql_real_escape_string($xml->query('//average_facebook_comments')->item($entry)->nodeValue);
$oneday_total_unique_authors = mysql_real_escape_string($xml->query('//total_unique_authors')->item($entry)->nodeValue);
$oneday_active_day_of_week = mysql_real_escape_string($xml->query('//active_day_of_week')->item($entry)->nodeValue);
$oneday_total_fans = mysql_real_escape_string($xml->query('//total_fans')->item($entry)->nodeValue);
$oneday_total_pages = mysql_real_escape_string($xml->query('//total_pages')->item($entry)->nodeValue);
$oneday_average_fans_per_page = mysql_real_escape_string($xml->query('//average_fans_per_page')->item($entry)->nodeValue);
$oneday_positive_sentiment = mysql_real_escape_string($xml->query('//positive_sentiment')->item($entry)->nodeValue);
$oneday_negative_sentiment = mysql_real_escape_string($xml->query('//negative_sentiment')->item($entry)->nodeValue);
$oneday_neutral_sentiment = mysql_real_escape_string($xml->query('//neutral_sentiment')->item($entry)->nodeValue);
$oneday_top_page_by_fans_title = mysql_real_escape_string($xml->query('//top_page_by_fans_title')->item($entry)->nodeValue);
$oneday_top_page_by_fans_url = mysql_real_escape_string($xml->query('//top_page_by_fans_url')->item($entry)->nodeValue);
$oneday_top_page_fans = mysql_real_escape_string($xml->query('//top_page_fans')->item($entry)->nodeValue);
$oneday_top_page_by_posts_title = mysql_real_escape_string($xml->query('//top_page_by_posts_title')->item($entry)->nodeValue);
$oneday_top_page_by_posts_url = mysql_real_escape_string($xml->query('//top_page_by_posts_url')->item($entry)->nodeValue);
$oneday_top_page_posts = mysql_real_escape_string($xml->query('//top_page_posts')->item($entry)->nodeValue);

$URL='https://www.viralheat.com/api/facebook/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=7';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$sevenday_total_facebook_comments = mysql_real_escape_string($xml->query('//total_facebook_comments')->item($entry)->nodeValue);
$sevenday_average_facebook_comments = mysql_real_escape_string($xml->query('//average_facebook_comments')->item($entry)->nodeValue);
$sevenday_total_unique_authors = mysql_real_escape_string($xml->query('//total_unique_authors')->item($entry)->nodeValue);
$sevenday_active_day_of_week = mysql_real_escape_string($xml->query('//active_day_of_week')->item($entry)->nodeValue);
$sevenday_total_fans = mysql_real_escape_string($xml->query('//total_fans')->item($entry)->nodeValue);
$sevenday_total_pages = mysql_real_escape_string($xml->query('//total_pages')->item($entry)->nodeValue);
$sevenday_average_fans_per_page = mysql_real_escape_string($xml->query('//average_fans_per_page')->item($entry)->nodeValue);
$sevenday_positive_sentiment = mysql_real_escape_string($xml->query('//positive_sentiment')->item($entry)->nodeValue);
$sevenday_negative_sentiment = mysql_real_escape_string($xml->query('//negative_sentiment')->item($entry)->nodeValue);
$sevenday_neutral_sentiment = mysql_real_escape_string($xml->query('//neutral_sentiment')->item($entry)->nodeValue);
$sevenday_top_page_by_fans_title = mysql_real_escape_string($xml->query('//top_page_by_fans_title')->item($entry)->nodeValue);
$sevenday_top_page_by_fans_url = mysql_real_escape_string($xml->query('//top_page_by_fans_url')->item($entry)->nodeValue);
$sevenday_top_page_fans = mysql_real_escape_string($xml->query('//top_page_fans')->item($entry)->nodeValue);
$sevenday_top_page_by_posts_title = mysql_real_escape_string($xml->query('//top_page_by_posts_title')->item($entry)->nodeValue);
$sevenday_top_page_by_posts_url = mysql_real_escape_string($xml->query('//top_page_by_posts_url')->item($entry)->nodeValue);
$sevenday_top_page_posts = mysql_real_escape_string($xml->query('//top_page_posts')->item($entry)->nodeValue);

$URL='https://www.viralheat.com/api/facebook/stats.xml?&profile_id='.$profile.'&api_key='.$api_key.'&days=30';
$doc = new DomDocument();
$doc->load($URL);
$xml = new DomXPath($doc);
$entry=0;

$profile_id = mysql_real_escape_string($xml->query('//id')->item($entry)->nodeValue);
$search_expression = mysql_real_escape_string($xml->query('//expression')->item($entry)->nodeValue);
$profile_name = mysql_real_escape_string($xml->query('//name')->item($entry)->nodeValue);

$thirtyday_total_facebook_comments = mysql_real_escape_string($xml->query('//total_facebook_comments')->item($entry)->nodeValue);
$thirtyday_average_facebook_comments = mysql_real_escape_string($xml->query('//average_facebook_comments')->item($entry)->nodeValue);
$thirtyday_total_unique_authors = mysql_real_escape_string($xml->query('//total_unique_authors')->item($entry)->nodeValue);
$thirtyday_active_day_of_week = mysql_real_escape_string($xml->query('//active_day_of_week')->item($entry)->nodeValue);
$thirtyday_total_fans = mysql_real_escape_string($xml->query('//total_fans')->item($entry)->nodeValue);
$thirtyday_total_pages = mysql_real_escape_string($xml->query('//total_pages')->item($entry)->nodeValue);
$thirtyday_average_fans_per_page = mysql_real_escape_string($xml->query('//average_fans_per_page')->item($entry)->nodeValue);
$thirtyday_positive_sentiment = mysql_real_escape_string($xml->query('//positive_sentiment')->item($entry)->nodeValue);
$thirtyday_negative_sentiment = mysql_real_escape_string($xml->query('//negative_sentiment')->item($entry)->nodeValue);
$thirtyday_neutral_sentiment = mysql_real_escape_string($xml->query('//neutral_sentiment')->item($entry)->nodeValue);
$thirtyday_top_page_by_fans_title = mysql_real_escape_string($xml->query('//top_page_by_fans_title')->item($entry)->nodeValue);
$thirtyday_top_page_by_fans_url = mysql_real_escape_string($xml->query('//top_page_by_fans_url')->item($entry)->nodeValue);
$thirtyday_top_page_fans = mysql_real_escape_string($xml->query('//top_page_fans')->item($entry)->nodeValue);
$thirtyday_top_page_by_posts_title = mysql_real_escape_string($xml->query('//top_page_by_posts_title')->item($entry)->nodeValue);
$thirtyday_top_page_by_posts_url = mysql_real_escape_string($xml->query('//top_page_by_posts_url')->item($entry)->nodeValue);
$thirtyday_top_page_posts = mysql_real_escape_string($xml->query('//top_page_posts')->item($entry)->nodeValue);

// query

#$result = mysql_query("SELECT * FROM vh_profile_full_details WHERE profile_id ='$profile_id' and search_term = '$search_expression' and profile_created = '$profile_created' and stats_processed = '$stats_processed' and deliver_email = '$deliver_email' and profile_public = '$profile_public' and profile_category = '$profile_category'");
#$num_rows = mysql_num_rows($result);
#if ($num_rows < 1) {

mysql_query("INSERT INTO vh_facebook_stats (profile_id, profile_name, search_expression, 1d_total_fb_comments, 1d_average_fb_comments, 1d_total_unique_authors, 1d_active_dow, 1d_total_fans, 1d_total_pages, 1d_average_fans_per_page, 1d_pos_sentiment, 1d_neg_sentiment, 1d_neut_sentiment, 1d_top_page_by_fans_title, 1d_top_page_by_fans_url, 1d_top_page_fans, 1d_top_page_by_posts_title, 1d_top_page_by_posts_url, 1d_top_page_posts, 7d_total_fb_comments, 7d_average_fb_comments, 7d_total_unique_authors, 7d_active_dow, 7d_total_fans, 7d_total_pages, 7d_average_fans_per_page, 7d_pos_sentiment, 7d_neg_sentiment, 7d_neut_sentiment, 7d_top_page_by_fans_title, 7d_top_page_by_fans_url, 7d_top_page_fans, 7d_top_page_by_posts_title, 7d_top_page_by_posts_url, 7d_top_page_posts, 30d_total_fb_comments, 30d_average_fb_comments, 30d_total_unique_authors, 30d_active_dow, 30d_total_fans, 30d_total_pages, 30d_average_fans_per_page, 30d_pos_sentiment, 30d_neg_sentiment, 30d_neut_sentiment, 30d_top_page_by_fans_title, 30d_top_page_by_fans_url, 30d_top_page_fans, 30d_top_page_by_posts_title, 30d_top_page_by_posts_url, 30d_top_page_posts) VALUES ('$profile_id', '$profile_name', '$search_expression', '$oneday_total_facebook_comments', '$oneday_average_facebook_comments', '$oneday_total_unique_authors', '$oneday_active_day_of_week', '$oneday_total_fans', '$oneday_total_pages', '$oneday_average_fans_per_page', '$oneday_positive_sentiment', '$oneday_negative_sentiment', '$oneday_neutral_sentiment', '$oneday_top_page_by_fans_title', '$oneday_top_page_by_fans_url', '$oneday_top_page_fans', '$oneday_top_page_by_posts_title', '$oneday_top_page_by_posts_url', '$oneday_top_page_posts', '$sevenday_total_facebook_comments', '$sevenday_average_facebook_comments', '$sevenday_total_unique_authors', '$sevenday_active_day_of_week', '$sevenday_total_fans', '$sevenday_total_pages', '$sevenday_average_fans_per_page', '$sevenday_positive_sentiment', '$sevenday_negative_sentiment', '$sevenday_neutral_sentiment', '$sevenday_top_page_by_fans_title', '$sevenday_top_page_by_fans_url', '$sevenday_top_page_fans', '$sevenday_top_page_by_posts_title', '$sevenday_top_page_by_posts_url', '$sevenday_top_page_posts', '$thirtyday_total_facebook_comments', '$thirtyday_average_facebook_comments', '$thirtyday_total_unique_authors', '$thirtyday_active_day_of_week', '$thirtyday_total_fans', '$thirtyday_total_pages', '$thirtyday_average_fans_per_page', '$thirtyday_positive_sentiment', '$thirtyday_negative_sentiment', '$thirtyday_neutral_sentiment', '$thirtyday_top_page_by_fans_title', '$thirtyday_top_page_by_fans_url', '$thirtyday_top_page_fans', '$thirtyday_top_page_by_posts_title', '$thirtyday_top_page_by_posts_url', '$thirtyday_top_page_posts')") or die(mysql_error());

echo 'Profile ID: ',$profile_id,'<br>';
echo 'Search expression: ',$search_expression,'<br>';
echo 'Profile name: ',$profile_name,'<br>';
echo 'All stats inserted!';
echo '<br>';

#}

#else {

#}

$profile_entry++;

}

mysql_close($conn);

$to = "simonjohnnicholasgeorge@gmail.com";
 $subject = "VH Update";
 $body = "Hi,\n\nHow are you?\n\nI have just finished running the Facebook_stats_all_days_fetch script.";
 if (mail($to, $subject, $body)) {
   echo("<p>Message successfully sent!</p>");
  } else {
   echo("<p>Message delivery failed...</p>");
  }

?> 	 

 </body>
</html>